# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_du_notify_session',
  :secret      => '8ef65bcef886bdfd7bc865784d363cebd12b51271fd39d2d3928d63dc663a3d02181fea758618813809a7da4df9f817700856ba2ad286023b275b661d8d0d26f'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
