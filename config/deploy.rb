set :application, "notfy.newdigitu.com"
set :repository,  "git@github.com:sandfish8/du_notify"
set :deploy_to, "/home/duwebadmin/notify.newdigitu.com"
set :scm, :git
set :user, "duwebadmin"
set :scm_username, "sandfish8"
set :use_sudo, false

role :app, "ps25540.dreamhost.com"
role :web, "ps25540.dreamhost.com"
role :db,  "ps25540.dreamhost.com", :primary => true