class ArticleProgress < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name 't_article_progress'
  
  belongs_to :course
  belongs_to :student
  
end
