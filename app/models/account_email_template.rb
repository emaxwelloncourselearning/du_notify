class AccountEmailTemplate < ActiveRecord::Base
  set_table_name "accounts_email_templates"
  
  belongs_to :email_template
end
