class EmailOption < ActiveRecord::Base
  
  def self.notify(debug = false, start_account = nil)
    # if start_account is supplied then all accounts starting with that one and later will be ran
    
    results = EmailResults.new
    DEBUG_LOGGER.info("Starting DEBUG of notify session") if debug
    
    found_account = start_account.nil?
    EmailOption.find(:all).each do |account_em|
        account = Account.find(account_em.account_id)
        found_account = true if account == start_account
        account_notify(account, debug, results)  if found_account
    end
    results
	end
  
  def self.account_notify(account, debug = false, results = EmailResults.new)
    
    # we should have no emails left in the cache if so raise error
    if EmailCache.find(:all).count > 0
      logger.error "Email Cache was not empty when starting a new account_notify pass. " +
                    "This could be caused if the previous email_notification job did not fully complete"
      EmailCache.purge
    end
     
    DEBUG_LOGGER.info("Examining the account: #{account.name} (#{account.id})") if debug
    # get account email options
    account_em = EmailOption.find(:first, :conditions => ["account_id = :account_id",
                                                        {:account_id => account.id}])
   
    unless account_em.blank?
      
      DEBUG_LOGGER.info("#{account.name} (#{account.id}) has email options") if debug
      
      find_lp_enroll(account, debug) if account_em.lp_enroll
      find_courses_due(account, debug) if account_em.courses_due?
      send_emails(results)
    end
    results 
  
  end
  
  def self.find_courses_due(account, debug = false)
    
    DEBUG_LOGGER.info("Looking for courses due")  if debug
    
    # get account email options
    account_em = EmailOption.find(:first, :conditions => ["account_id = :account_id",
                                                        {:account_id => account.id}]) 
                                                         
    account.students.each do |student|
      
      DEBUG_LOGGER.info("Examining student: Student id is #{student.id}")  if debug
      valid_email = student.valid_email_address?
      
      unless valid_email
        DEBUG_LOGGER.info("The email address - #{student.email_address} - for student #{student.id} is not valid")  if debug
        next
      end
      
      DEBUG_LOGGER.info("The email address - #{student.email_address} - for student #{student.id} is valid")  if debug
      
      StudentLearningPlan.find(:all, 
            :conditions =>  ["student_id = :student_id AND is_deleted != 'Y' AND is_completed != 'Y'",
                            {:student_id => student.id}]).each do |slp|
        
        DEBUG_LOGGER.info("examining student_learning_plan (#{slp.student_learning_plan_id})")  if debug
        
        # HERE WOULD BE A CHECK TO MAKE SURE LEARNING PLAN ISNT PAST DUE
        # IF THE LEARNING PLAN END HAS PASSED OR THE LP IS INACTIVE THEN DON'T BOTHER CHECKING
        
        next if slp.learning_plan.nil?
        next if slp.learning_plan.is_active == 'N'
        next if slp.learning_plan.due_date < Date.today
        
        slp.learning_plan.curriculums.each do |cur|
          cur.courses.each do |course|
            
            DEBUG_LOGGER.info("Examining course id #{course.id}") if debug
            
            # if course has already been completed then skip this course and move on
            if course.completed?(slp.learning_plan, student)
              DEBUG_LOGGER.info("Course id #{course.id} has already been complated. Moving to next course") if debug
              next
            end
            
            course_due_date = slp.learning_plan.due_date_for(course)
            DEBUG_LOGGER.info("The due date for course #{course.id} is #{course_due_date.to_s}") if debug
            
            # loop through courses_due_options to check for any courses due in a certain amount of time
            account_em.courses_due_options.each do |template|
              
              days = template.days
              target = Date.today.advance(:days => days)
              
              # if the due_date is our target date then write to cache for later sending
              if course_due_date.to_datetime === target
                # assign an email template
                email_template = template.for_account(account) 
                EmailCache.create(  :student => student, 
                                    :learning_plan => slp.learning_plan, 
                                    :course => course, 
                                    :date => course_due_date, 
                                    :email_template => email_template)
                DEBUG_LOGGER.info("found course due in #{days} days.  Added entry to email cache") if debug
              end 
            end
          end
        end
      end
    end
  end  


 def self.find_lp_enroll(account, debug = false)
    
    DEBUG_LOGGER.info("Looking for new learning plan enrollments") if debug
    
    account.students.each do |student|
      
      DEBUG_LOGGER.info("Examining student: Student id is #{student.id}")  if debug
      valid_email = student.valid_email_address?
      
      unless valid_email
        DEBUG_LOGGER.info("The email address - #{student.email_address} - for student #{student.id} is not valid")  if debug
        next
      end
      
      student.student_learning_plans.each do |slp|
        
        DEBUG_LOGGER.info("Found student_learning_plan. student_learning_plan_id is #{slp.student_learning_plan_id}") if debug
        
        # was the student_learning_plan modified yesterday? if so then it's just been created and we will notify everyone
        yesterday = Date.yesterday
        
        if yesterday === slp.last_modified.to_datetime
          email_template = EmailTemplate.find_by_message_type(EmailTemplate::LP_ENROLL).for_account(account)
          em = EmailCache.create( :student => student, :learning_plan => slp.learning_plan, :email_template => email_template )
          DEBUG_LOGGER.info("Found a newly enrolled student! Added entry to email cache") if debug
        end
      end
    end 
  end  

  def self.send_emails(results = EmailResults.new)
    
    while(email = Email.get_email) do
      
      should_send_email = Setting.find_by_key("send_email").value == "1" ? true : false
      
      # send the email, wrapping in exception handling so we can keep rolling if we hit errors for any
      # one address
              
      _email = nil
      begin
        if email.template.message_type == EmailTemplate::LP_ENROLL
          _email = should_send_email ? DigituMailer.deliver_lp_enroll(email) : DigituMailer.create_lp_enroll(email)
        elsif email.template.message_type == EmailTemplate::COURSE_DUE_30 or email.template.message_type == EmailTemplate::COURSE_DUE_15 or email.template.message_type ==  EmailTemplate::COURSE_DUE_5 or email.template.message_type == EmailTemplate::COURSE_DUE_1
          _email = should_send_email ? DigituMailer.deliver_course_due(email) : DigituMailer.create_course_due(email)
        end
      rescue => e
        EMAIL_LOGGER.error("Error when sending email to student #{student.id}: #{e}")
      rescue Timeout::Error => e
        EMAIL_LOGGER.error("Timing out when sending email to student #{student.id}: #{e}")
      end
      
      EMAIL_LOGGER.info(wrap_log_entry(should_send_email, _email.encoded)) unless email.nil?
      
      EmailHistory.create(  :student_id => email.student.id, 
                            :template_id => email.template.id, 
                            :date => DateTime.now,
                            :email_was_sent => should_send_email ? 1 : 0)
      results.add(email.student.account.name)
    end
  end
  
  def self.wrap_log_entry(was_sent, string)
    sent_text = was_sent ? "EMAIL SENT" : "EMAIL CREATED"
    email_begin = "\n\n***********************************************\n" +
                          "*****  BEGIN -- #{sent_text} \n" +
                          "***********************************************\n\n"
    email_end = "\n\n***********************************************\n" +
                          "*****  END -- #{sent_text}\n" +
                          "***********************************************\n\n"
    email_begin + string + email_end
  end
  
  def courses_due_options
    options = []
    options << EmailTemplate.find_by_message_type(EmailTemplate::COURSE_DUE_30) if course_due_30?  
    options << EmailTemplate.find_by_message_type(EmailTemplate::COURSE_DUE_15) if course_due_15?  
    options << EmailTemplate.find_by_message_type(EmailTemplate::COURSE_DUE_5) if course_due_5?
    options << EmailTemplate.find_by_message_type(EmailTemplate::COURSE_DUE_1) if course_due_1?
    options
  end
  
  def courses_due?
    true if course_due_30? || course_due_15? || course_due_5? || course_due_1?
  end
  
  def self.get(account)
    EmailOption.find_by_account_id(account.id)
  end
   
end
