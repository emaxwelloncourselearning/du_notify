class CourseMetadata < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_course_metadata"
  set_primary_key "course_metadata_id"
  
end
