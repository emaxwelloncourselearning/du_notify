class StudentLearningPlan < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_students_learning_plans"
  set_primary_key "student_learning_plan_id"
  
  belongs_to :student
  belongs_to :learning_plan
end
