class AccountCustomText < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_account_custom_text"
  
  # hack to work around the table having a 'type' column which breaks ActiveRecord
  set_inheritance_column :ruby_type
  
  belongs_to :account
end
