class Setting < ActiveRecord::Base

 def self.get_admins
  admins = []
  Setting.find(:all, :conditions => ["`key` = ?", "admin"]).each do |admin|
    admins << admin.value
  end
  admins
 end

 def value?
  value == "1"
 end

end
