class Course < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name 'assets'
  
  has_many :learning_plan_course_due_dates
  has_and_belongs_to_many :curriculums,
                          :join_table => "t_xref_curricula_x_courses",
                          :foreign_key => "asset_id",
                          :conditions =>  "t_xref_curricula_x_courses.is_deleted != 'Y' AND " +
                                          "t_curricula.is_deleted != 'Y' AND " +
                                          "t_curricula.is_active != 'N'",
                          :order => "t_xref_curricula_x_courses.position ASC"
  belongs_to :account
                           
  def metadata
    CourseMetadata.find(:first, :conditions =>  ["course_id = :course_id", 
                                                {:course_id => self.file_name}])
  end
  
  def tests
    DuTest.find(:all, :conditions => "course_id = '#{file_name}'")
  end
  
  def completed?(learning_plan, student)
  # return true if course has been completed
  #
  # logic is:  
  # examine t_tests table
  # here's Keith's criteria:   
  #           you need a t_tests row for that student for that course that is pre_or_post = POST and is_completed = Y 
  #           and date_completed is after or = learning plan start date and before or = to course/lp due date
  # =>        and result = 'Passed'
    
    test = student.du_tests.find(:first, :conditions => [ "course_id = :course_id AND " +
                                                          "pre_or_post = 'POST' AND " +
                                                          "is_completed = 'Y' AND " +
                                                          "result = 'PASSED' AND " +
                                                          "date_completed >= '#{learning_plan.start_date.to_s(:db)}' AND " +
                                                          "date_completed <= '#{learning_plan.due_date.to_s(:db)}'",
                                                          {:course_id => self.file_name}])
    !test.nil? 
  end

end
