class DuTest < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_tests"
  
  belongs_to :student
  belongs_to :account
  belongs_to :course
  belongs_to :learning_plan
  
end
