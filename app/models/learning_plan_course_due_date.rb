class LearningPlanCourseDueDate < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_learning_plan_course_due_dates"
  
  belongs_to :learning_plan
  belongs_to :course
  
end
