class ContentProvider < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_accounts"
  
  # hack to work around the table having a 'type' column which breaks ActiveRecord
  set_inheritance_column :ruby_type 
  
  has_and_belongs_to_many :accounts, 
                          :join_table => "t_xref_organizations_x_content_providers",
                          :foreign_key => "content_provider_id", 
                          :association_foreign_key => "organization_id",
                          :conditions => "active != 'N'",
                          :order => "t_accounts.name ASC"
  
end
