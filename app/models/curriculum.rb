class Curriculum < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_curricula"
  set_primary_key "curriculum_id"
  
  has_and_belongs_to_many :learning_plans,
                          :join_table => "t_xref_learning_plans_x_curricula",
                          :conditions => "t_xref_learning_plans_x_curricula.is_deleted != 'Y' " + 
                                          "AND t_learning_plans.is_deleted != 'Y' " +
                                          "AND t_learning_plans.is_active != 'N'",
                          :order => "t_xref_learning_plans_x_curricula.position ASC"
  
  has_and_belongs_to_many   :courses,
                            :join_table => "t_xref_curricula_x_courses",
                            :association_foreign_key => "asset_id",
                            :conditions =>  "t_xref_curricula_x_courses.is_deleted != 'Y' AND " +
                                            " assets.is_deleted != 'Y'",
                            :order => "t_xref_curricula_x_courses.position ASC"
end
