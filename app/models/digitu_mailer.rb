class DigituMailer < ActionMailer::Base
  
  def lp_enroll(email, sent_at = Time.now)
    subject    email.subject
    recipients email.recipients
    cc         email.cc
    bcc        email.bcc
    from       email.from
    sent_on    sent_at
    body       :email => email
  end
  
  def course_due(email, sent_at = Time.now)
    subject    email.subject
    recipients email.recipients
    cc         email.cc
    bcc        email.bcc
    from       email.from
    sent_on    sent_at
    body        :email => email
  end
  
  
end
