class AccountSetting < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_account_settings"
  set_primary_key "account_settings_id"
  
  belongs_to :account
  
end
