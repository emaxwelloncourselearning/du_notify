class Email < EmailCache
  
  attr_accessor :courses
  attr_accessor :learning_plans
  attr_accessor :template
  attr_accessor :student
  
  def initialize
    @courses = []
    @learning_plans = []
  end
  
  def inspect
    super.inspect
  end
  
  def self.get_email
    # snags the first email from the email_caches table
    # consolidate all courses and learning plans into arrays
    # removing repeasts as they get put into the array
    
    email = Email.new
    
    first_email = EmailCache.find(:first)
    return false if first_email.nil?
    
    email.template = first_email.email_template
    email.student = first_email.student
    email.assimilate(first_email)
    
    others = EmailCache.find(:all, 
        :conditions => [ "student_id = :student AND email_template_id = :email_template",
                        { :student => email.student, :email_template => email.template}]) 
    others.each { |ot| email.assimilate(ot) }
    email
  end
  
  def assimilate(ec)
  # capture any resources that we might have multiple instances of (e.g. course, learning_plans)
  # and remove individual instance 
   
   case ec.email_template.message_type
   when EmailTemplate::LP_ENROLL
      learning_plans <<  ec.learning_plan
   when EmailTemplate::COURSE_DUE_30, EmailTemplate::COURSE_DUE_15, EmailTemplate::COURSE_DUE_5, EmailTemplate::COURSE_DUE_1
      courses << { :course => ec.course, :due_date => ec.date }
   end
   ec.destroy
  end
  
  def subject
    
    _app_name = student.account.application_name
    app_name = _app_name.blank? ? "Digital University" : _app_name
    app_name + ": " + template.subject
  end

  def recipients
     
     if Setting.find_by_key("email_override").value == "1"
       recipient = Email.comma_separated_into_array(Setting.find_by_key("email_override_address").value)
     else
       recipient = Email.comma_separated_into_array("#{student.first_name} #{student.last_name} <#{student.email_address.strip}>")
     end
     recipient
  end
  
  def from
    Email::account
  end
  
  def reply_to
    Email::account
  end
  
  def cc
    admin = nil
    
    unless Setting.find_by_key("email_override").value == "1"
      em = EmailOption.get(student.account)
      unless em.nil?
        if em.cc_org_admin?
          admin = Email.comma_separated_into_array(em.org_admin.strip)
        end
      end
    end
    admin
  end
  
  def bcc
    bcc = nil
    if Setting.find_by_key("bcc_admins").value == "1"
      bcc = Setting.get_admins
    end
    bcc
  end
  
  def self.notify_admins(message)
    
    recipients = []
    Setting.find(:all, :conditions => "`key` = 'admin'").each do |admin|
      recipients << admin.value
    end
    
    unless recipients.empty?
      options = {   :recipients => recipients,
                    :subject => "Administrative message from elearning-addon.newdigitu.com",
                    :message => message }
     _email = DigituMailer.deliver_administrator_message(options)
    end
  end
  
  # SENDING EMAIL FROM MULTIPLE ADDRESSES
  
  def self.switch_account(account, password = nil)
    ActionMailer::Base.smtp_settings[:user_name] = account
    ActionMailer::Base.smtp_settings[:pasword] = password unless password.nil?
    
    ActionMailer::Base.smtp_settings[:user_name]
  end
  
  def self.account
    ActionMailer::Base.smtp_settings[:user_name]
  end
  
  def self.accounts
    ["notification@digitu.com", "notification2@digitu.com", "notification3@digitu.com"] 
  end
  
  def self.next_account(current_account)
  # deliver the next account given the current one
    
    next_account = nil
    accounts.each_index do |index|
      if accounts[index] == current_account
        #if we are at the end, then pass the first
        if index == (accounts.size - 1)
          next_account = accounts[0]
        else #otherwise pass the next
          next_account = accounts[index + 1]
        end
      end
    end
    next_account
  end
  
  def self.switch_next_account
    switch_account(next_account(account))
  end
  
  private
  def self.comma_separated_into_array(string)
    # if string contains comma(s) then return array
    # else return the string
    
    parts = string
    
    if string.include?(',')
      parts = string.split(',')
    
      # strip space from beginning and end of each part
      parts.collect! { |_p| _p.gsub(/^[ ]+/, '') }
      parts.collect! { |_p| _p.gsub(/[ ]+$/, '') }   
    end
    parts 
  end
  
end
