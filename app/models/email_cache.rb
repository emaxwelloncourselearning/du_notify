class EmailCache < ActiveRecord::Base

belongs_to :student
belongs_to :learning_plan
belongs_to :email_template
belongs_to :course

  def self.purge
    EmailCache.find(:all).each { |ec| ec.destroy }
  end
   
end
