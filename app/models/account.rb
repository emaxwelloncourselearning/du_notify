class Account < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_accounts"
  
  # hack to work around the table having a 'type' column which breaks ActiveRecord
  set_inheritance_column :ruby_type
  
  # relationships
  
  has_and_belongs_to_many :content_providers, 
                          :join_table => "t_xref_organizations_x_content_providers",
                          :foreign_key => "organization_id",
                          :association_foreign_key => "content_provider_id",
                          :conditions => "active != 'N'"
  
  has_one :account_setting, 
          :foreign_key => "account_id",
          :conditions => "is_deleted != 'Y'"
  
  has_many  :students, 
            :foreign_key => "account_id",
            :conditions => "is_enabled = 1 AND is_deleted != 'Y'"
  
  has_many  :courses,
            :conditions => "file_type = 'course' AND is_deleted != 'Y'"
  
  has_many  :learning_plans,
            :conditions => "is_deleted != 'Y' AND is_active != 'N'"
            
  has_many  :curriculums,
            :foreign_key => "account_id",
            :conditions =>  "is_deleted != 'Y'"
            
 has_many :account_custom_texts
 
 
 # constants
 
 CUES_APP_NAME = "CUES Online University Plus" 
 DU_APP_NAME = "Digital University Plus"
 NASCUS_APP_NAME = "NASCUS Online University" 
  
  
  # methods
  
  def self.get(id)
     query = "SELECT * FROM t_accounts WHERE id = id"
     MigratedOrganization.find_by_sql(query)
  end
  
  def self.get_all
    query = "SELECT * FROM t_accounts ORDER By name"
    MigratedOrganization.find_by_sql(query)
  end
  
  def level
    
    unless self.account_setting.nil?
      if self.account_setting.allow_administrators_access_to_settings == 'Y'
        return "Pro"
      elsif self.account_setting.allow_students_access_to_learning_plans == 'Y'
        return "Plus"
      else
        return "Basic"
      end
    else
      return "no settings"
    end
  end
  
  def num_students
    
    query = "SELECT COUNT(*) as student_count FROM t_students WHERE account_id = #{self.id}"
    num_students_result = Account.find_by_sql(query)
    
    unless num_students_result.first.nil?
      return num_students_result.first.student_count
    else
      return 0
    end
  end
  
  def application_name
    account_custom_texts.find(:first, :conditions => "type = 'application_name'").value
  end
  
  def login_page
    "https://secure.pluslms.com/university/login.xml?for=#{self.url_name}"
  end
  
end
