class Student < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_students"
  
  belongs_to :account, :foreign_key => "account_id"
  has_many  :student_learning_plans,
            :conditions => "t_students_learning_plans.is_deleted != 'Y'"
  has_many  :learning_plans, 
            :through => :student_learning_plans,
            :conditions =>  "t_learning_plans.is_deleted != 'Y' AND " +
                            "t_learning_plans.is_active != 'N'"
  has_many :du_tests
  has_one :administrator,
          :conditions => "t_administrators.is_deleted != 'Y'"
                            
  def valid_email_address?
    case email_address
    when ""
      valid = false
    else
      valid = true
    end
    valid
  end
end
