class LearningPlan < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_learning_plans"
  set_primary_key "learning_plan_id"
  
  belongs_to :account
  
  has_many  :student_learning_plans,
            :conditions =>  "t_students_learning_plans.is_deleted != 'Y'"
  
  has_many  :students, 
            :through => :student_learning_plans,
            :conditions => "t_students.is_deleted != 'Y' AND is_enabled = 1" 
            
  has_many :learning_plan_course_due_dates
  
  has_and_belongs_to_many :curriculums,
                          :join_table => "t_xref_learning_plans_x_curricula",
                          :conditions =>  "t_xref_learning_plans_x_curricula.is_deleted != 'Y' AND " +
                                          "t_curricula.is_deleted != 'Y' AND " + 
                                          "t_curricula.is_active != 'N'",
                          :order => "t_xref_learning_plans_x_curricula.position ASC"  
  
  
  def due_date_for(course)
    
    # try and get course specific due date
    _due_date = nil
    lpccd = learning_plan_course_due_dates.find(:first, 
                                                      :conditions => ["course_id = :course_id",
                                                                      {:course_id => course.file_name}])
    _due_date = lpccd.due_date unless lpccd.nil? 
    
    # else the due_date is determined by the learning plan
    _due_date = due_date if _due_date.nil?
    _due_date 
  end
  
  def course_count
  # how many courses are associated with this lp?  This is the sum of all courses in each associated curriculum
    num_courses = 0
    curriculums.each do |cur|
      cur.courses.each do |course|
        num_courses += 1
      end
    end
    num_courses
  end
                         
end
