class Administrator < ActiveRecord::Base
  belongs_to :student
  belongs_to :account
  has_one :administrator_right, 
          :conditions => "t_administrator_rights.is_deleted != 'Y'"
  
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name 't_administrators'
end
