class AdministratorRight < ActiveRecord::Base
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name 't_administrator_rights'
  belongs_to :administrator
  set_primary_key 'administrator_id'
end
