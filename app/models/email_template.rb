class EmailTemplate < ActiveRecord::Base 
  
  has_many :account_email_templates
  
  LP_ENROLL = "lp_enroll"
  COURSE_DUE_30 = "course_due_30"
  COURSE_DUE_15 = "course_due_15"
  COURSE_DUE_5 = "course_due_5"
  COURSE_DUE_1 = "course_due_1"
  
  def for_account(account)
  # return the email_template that a student of a certain account should receive
  # if the accounts has a custumized version of the template type then return that
  # otherwise return the orginal non-customized template
    
     # if account has a custom template then return that
     aet = self.account_email_templates.find(:first, :conditions => ["account_id = :account_id AND email_template_id = :et_id",
                                                          {:account_id => account.id, :et_id => self.id}])
     email_template = aet.email_template unless aet.nil?                                                  
     # else return generic template
     if email_template.nil?
        email_template = original
     end                                                     
     email_template 
  end
  
  def original
    EmailTemplate.find(:first, :conditions => ["message_type = :msg_type AND original = :original",
                                      {:msg_type => self.message_type, :original => true}])
  end
  
  def days
    case message_type
    when COURSE_DUE_30
      days = 30
    when COURSE_DUE_15
      days = 15
    when COURSE_DUE_5
      days = 5
    when COURSE_DUE_1
      days = 1
    end
    days
  end
  
  def subject
    case message_type
    when LP_ENROLL
      "You have been enrolled in training."
    when COURSE_DUE_30, COURSE_DUE_15, COURSE_DUE_5, COURSE_DUE_1
      "You have assigned training due in #{days_formatter}."
    end
  end
  
  def days_formatter
  # writes language when describing days
  # e.g.  "5 days" if days = 5
  #       "1 day" if days = 1
    
    if days == 1
      days.to_s + " day"
    elsif days > 1
      days.to_s + " days"
    end
  end

end
