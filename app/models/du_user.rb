class DuUser < Student
  
  def self.authenticate(username, password, message)
    
    # lookup against t_administrators to see if this person has access
    # also verify against t_administrator_right that they have the has_access_to_administrators privilege
    
    # person = Student.find(:first, :conditions => ["username = ?", "jeaninebaderadmin"], :joins => "as st inner join t_administrators as ad on st.id = ad.student_id")  
    # person = Student.find(:first, :conditions => ["username = ? AND t_administrators.student_id = t_students.id", "jeaninebaderadmin"])
    can_authenticate = false
    student = Student.find(:first, :conditions => ["username = ?", username])
    
    unless student.nil?
    	admin = Administrator.find(:first, :conditions => ["student_id = ?", student.id])
    	
    	unless admin.nil?
        
        # does the admin have the appropriate rights?
        no_rights_text = "Login failed. Account does not have privilege to modify other admins." # "User does not have privilege to modify other admins"
        unless admin.administrator_right.nil?
          if admin.administrator_right.has_access_to_administrators == 'Y'
            if student.password == password
              can_authenticate = true
            else
              message.replace(no_rights_text)
            end
          else
            message.replace(no_rights_text) 
          end
        end
      else
        message.replace("Login failed. Please try again.") # "User is not an administrator"
      end
    	
    else
    	message.replace("Authentication failed. Please try again.") # "Could not locate the username"
    end
                                   
    can_authenticate ? student : nil
  end
  
end
