class ArticleFeedback < ActiveRecord::Base
  
  establish_connection "du_production_#{RAILS_ENV}"
  set_table_name "t_article_feedback"
  set_primary_key "article_feedback_id"
  
  belongs_to :student
  
end
