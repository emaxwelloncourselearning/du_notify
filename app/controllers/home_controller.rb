class HomeController < ApplicationController
  
  before_filter :check_for_authentication, :except => [ :login, :logout ]
  
  def index
    
    # if there is not an email_option record for this org then send them to the agreement page first
    # once the agreement page is agreed to a row will be created in the email_option table
    # and they shouldn't have to see that page again
    
    @em = EmailOption.find(:first, :conditions => "account_id = #{@user.account.id}")
    redirect_to :controller => :home, :action => :agreement and return if @em.nil?
    
  end

  def agreement
    unless params[:agree].nil?
      case params[:agree]
      when "1"
        # create an em options record for this org if one does not exist already
        em = EmailOption.find_by_account_id(@user.account.id)
        if em.nil?
          EmailOption.create( :account_id => @user.account.id )
        end
        redirect_to :controller => :home, :action => :index
      when "0"
        redirect_to :controller => :home, :action => :logout
      end
    end
  end
  
  def login
    
    @user = nil
    @show_cancel = false
    @message = ""
    @username = params[:username]
    @password = params[:password]
    
    destination_page = get_destination_page
                
    # if we received a Cancel command, then take user back to last page other than this one
    if params[:cancel]
      redirect_to(destination_page) and return
    end
    
    # try to authenticate
    unless @username.blank?
      user = DuUser.authenticate(@username, @password, @message)  
    end
      
    unless user.blank? # if successful
       session[:user_id] = user.id # populate session with user object
       # forward user to the last page they were trying to go to that was not the login action
         redirect_to destination_page and return
     end
  end

  def logout
    @user = nil
    session[:user_id]  = nil
    redirect_to :controller => :home, :action => :index
  end
  
  def update_email_settings
    
    eo = EmailOption.find_by_account_id(params[:id])
    
    # if the emailOption doesn't exist
    if eo.nil?
      EmailOption.create( :account_id => params[:id].to_i, 
                          :lp_enroll => params[:lp_enroll] == "1" ? true : false,
                          :course_due_30 => params[:course_due_30] == "1" ? true : false,
                          :course_due_15 => params[:course_due_15] == "1" ? true : false,
                          :course_due_5 => params[:course_due_5] == "1" ? true : false,
                          :course_due_1 => params[:course_due_1] == "1" ? true : false,
                          :cc_org_admin => params[:cc_org_admin] == "1" ? true : false,
                          :org_admin => params[:org_admin])
    else #update
      eo.lp_enroll = params[:lp_enroll] == "1" ? true : false
      eo.course_due_30 = params[:course_due_30] == "1" ? true : false
      eo.course_due_15 = params[:course_due_15] == "1" ? true : false
      eo.course_due_5 = params[:course_due_5] == "1" ? true : false
      eo.course_due_1 = params[:course_due_1] == "1" ? true : false
      eo.cc_org_admin = params[:cc_org_admin] == "1" ? true : false
      eo.org_admin = params[:org_admin]
      eo.save!
    end
    respond_to do |format|
      format.js { render :json, :inline => "{\"status\": true}" }
    end
  end
  
  private
  def get_destination_page
    # find destingation page and determine to show cancel button or not
    
    @show_cancel = false
    destination_page = ""
     session[:history].reverse_each do |hp|
       if not hp.include?("login") and not hp.include?("logout") and not hp.include?("unauthorized")
         destination_page = hp
         @show_cancel = true
         break
       end
     end
     destination_page = destination_page.blank? ? "/home" : destination_page
  end
  
  def check_for_authentication
    if @user.blank?
      redirect_to(:controller => :home, :action => :login)
    end
  end

end
