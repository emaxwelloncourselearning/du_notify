# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password
  
  before_filter :track_last_page, :get_user, :check_for_site_off
  filter_parameter_logging :password
  
  def track_last_page
    session[:history] ||= []
    session[:history] << request.request_uri
  end
  
  def get_user
    @user = DuUser.find(session[:user_id]) unless session[:user_id].blank? 
  end
  
  def check_for_site_off
    @site_off = false
    if @site_off
      render :file => "#{RAILS_ROOT}/public/site_down.html"
    end
  end
  
end
